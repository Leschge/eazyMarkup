$( document ).ready(function() {

    syntax();

    function syntax(){
        // Escape < and > in all elements //
        $("cmd").each(function(){

            // Remove auto close tags from browser
            /*
            str = $(this).html();
            while(str.indexOf("</") != -1)
            {
                open = str.indexOf("</");
                close = str.indexOf(">",open);
                str = str.substring(0,open)+str.substring(close+1);
            }*/
            $(this).html( $(this).html().replace(/<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g,"").replace(/</g, '&lt;').replace(/>/g, '&gt;') );
            
        });

        $("file").each(function(){
            $(this).html( $(this).html().replace(/<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g,"").replace(/</g, '&lt;').replace(/>/g, '&gt;') );
        });

        $("inline").each(function(){
            $(this).html( $(this).html().replace(/<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g,"").replace(/</g, '&lt;').replace(/>/g, '&gt;') );
        });


        // Generate CMD //
        $("cmd").each(function(){
            var cmd = $(this).html()//.replace(/</g, '&lt;').replace(/>/g, '&gt;')
            var $cmdcopy = $("<cmd-copy></cmd-copy>");
            var $cmdtxt = $("<cmd-txt>"+cmd+"</cmd-txt>");

            $(this).html("");
            $(this).append($cmdcopy);
            $(this).append($cmdtxt);
        });


        // Genrate Youtube Video //
        $("youtube").each(function(){
            var link = $(this).html()
            var $youtube = $("<iframe class='youtube' src='"+link+"' allowfullscreen></iframe>");
            $(this).after($youtube);
            $(this).remove();
        });


        // Genrate Spoiler //
        $("spoiler").each(function(){
            var $spoilerExp = $("<spoiler-expand></spoiler-expand>");
            $(this).after($spoilerExp);
            $($spoilerExp).val("hidden");
        });

        $("spoiler-expand").on('click', function(e){
            if($(this).val() == "hidden" )
            {
                $(this).prev().css('max-height', '9999999px');
                $(this).css('background-image', 'url(assets/symUpSmall.png)');
                $(this).val("visible");
            }
            else{
                $(this).prev().css('max-height', '');
                $(this).css('background-image', 'url(assets/symDownSmall.png)');
                $(this).val("hidden");
            }
        });
    }


    // COPY TEXT FEATURE //
    $("inline").on('click', function(e){
        copyText(this);
    });

    $("fileheader").on('click', function(e){
        copyText($(this).next());
    });

    $("cmd-copy").on('click', function(e){
        copyText($(this).next());
    });



    function copyText(element){
        var text = $(element).clone().find('br').prepend('\r\n').end().text()
        element = $('<textarea>').appendTo($(element)).val(text).select()
        document.execCommand('copy')
        element.remove()

        displayCopied();
    }

    function displayCopied(){
        var $popup = $("<div class='popupCopied'>Copied!<div>");
        $("body").append($popup);

        $popup.delay(1000).fadeOut(500); 
    }

});